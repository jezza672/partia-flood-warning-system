from floodsystem.station import MonitoringStation
from floodsystem.utils import sorted_by_key
from floodsystem.flood import stations_level_over_threshold
from floodsystem.flood import stations_highest_rel_level



Fake_stations_for_testing = [
    MonitoringStation(4000, 444, "bathuni", (51.378290, -2.326485), None, "ariver", "atown"), 
    MonitoringStation(6000, 666, "standrewsuni", (56.342094,-2.796016), (0.1, 3), "briver", "btown"), 
    MonitoringStation(1000, 111, "botgardens", (52.197735, 0.127588), (3, 0.1), ["criver", "friver"], "ctown"), 
    MonitoringStation(3000, 333,"pemcolloxf", (51.750242, -1.257518), (0, 1), "driver", "dtown"), 
    MonitoringStation(2000, 222, "myhouse", (51.61, -0.26), None, "eriver", "etown"), 
    MonitoringStation(5000, 555, "uniofdurham", (54.765196, -1.583177), (0, 1), "ariver", "ftown")
    ]

Fake_stations_for_testing[0].latest_level = 12
Fake_stations_for_testing[1].latest_level = 3
Fake_stations_for_testing[2].latest_level = 54
Fake_stations_for_testing[3].latest_level = 2
Fake_stations_for_testing[4].latest_level = 34
Fake_stations_for_testing[5].latest_level = 0.7

def test_stations_level_over_threshold():
    outputs = stations_level_over_threshold(Fake_stations_for_testing, 1)
    assert (Fake_stations_for_testing[1], 1) not in outputs
    assert (Fake_stations_for_testing[3], 2) in outputs
    assert (Fake_stations_for_testing[5], 0.7) not in outputs
    
def test_stations_highest_rel_level():
    highest_ones = stations_highest_rel_level(Fake_stations_for_testing, 3)
    assert len(highest_ones) == 3
    for i in range(len(highest_ones)-1):
        assert highest_ones[i+1].relative_water_level() < highest_ones[i].relative_water_level()