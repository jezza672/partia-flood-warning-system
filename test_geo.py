"""Unit test for geo"""
'''(station_id, measure_id, label, coord, typical_range,
                 river, town)'''
import floodsystem.geo as geo
from floodsystem.station import MonitoringStation
from floodsystem.utils import calculate_distance
from floodsystem.utils import sorted_by_key

Fake_stations_for_testing = [
    MonitoringStation(4000, 444, "bathuni", (51.378290, -2.326485), None, "ariver", "atown"), 
    MonitoringStation(6000, 666, "standrewsuni", (56.342094,-2.796016), (0.1, 3), "briver", "btown"), 
    MonitoringStation(1000, 111, "botgardens", (52.197735, 0.127588), (3, 0.1), ["criver", "friver"], "ctown"), 
    MonitoringStation(3000, 333,"pemcolloxf", (51.750242, -1.257518), (0.05, 1.2), "driver", "dtown"), 
    MonitoringStation(2000, 222, "myhouse", (51.61, -0.26), None, "eriver", "etown"), 
    MonitoringStation(5000, 555, "uniofdurham", (54.765196, -1.583177), (1, 0.3), "ariver", "ftown")
    ]

""" Test point will be the UL """
p_test = (52.197735, 0.107793)

'''test stations by distance algorithm in geo'''
def test_stations_by_distance():
    dist = geo.stations_by_distance(Fake_stations_for_testing, p_test)
    assert  len(dist) ==  len(Fake_stations_for_testing)
    assert  dist[0][0] == Fake_stations_for_testing[2]

'''test stations within radius algorithm'''
def test_stations_within_radius():
    r = 15
    nearby_stations = geo.stations_within_radius(Fake_stations_for_testing, p_test, r)
    
    assert type(nearby_stations) == type([])
    
    assert geo.stations_within_radius(Fake_stations_for_testing, p_test, r)[0] == Fake_stations_for_testing[2]

def test_rivers_with_station():
    rivers = geo.rivers_with_station(Fake_stations_for_testing)
    shortrivers = geo.rivers_with_station(Fake_stations_for_testing[0:3])
    '''called it onlyoneriver but theres two! ironic?'''
    onlyoneriver = geo.rivers_with_station([Fake_stations_for_testing[2]])
    assert "ariver" in rivers
    assert "briver" in rivers
    assert "criver" in rivers
    assert "driver" in rivers
    assert "eriver" in rivers
    assert "friver" in rivers
    assert "ariver" in shortrivers
    assert "briver" in shortrivers
    assert "criver" in shortrivers
    assert "driver" not in shortrivers
    assert "eriver" not in shortrivers
    assert "friver" in shortrivers
    assert "ariver" not in onlyoneriver
    assert "briver" not in onlyoneriver
    assert "criver" in onlyoneriver
    assert "driver" not in onlyoneriver
    assert "eriver" not in onlyoneriver
    assert "friver" in onlyoneriver
    assert len(rivers) == 6
    assert len(shortrivers) == 4
    assert len(onlyoneriver) == 2
 
def test_stations_by_river():
    fakedict = geo.stations_by_river(Fake_stations_for_testing)
    assert Fake_stations_for_testing[0] in fakedict['ariver']
    assert Fake_stations_for_testing[5] in fakedict['ariver']
    assert len(fakedict['ariver']) == 2
    assert Fake_stations_for_testing[1] in fakedict['briver']
    assert Fake_stations_for_testing[2] in fakedict['criver']
    assert Fake_stations_for_testing[2] in fakedict['friver']
    assert Fake_stations_for_testing[3] in fakedict['driver']
    assert Fake_stations_for_testing[4] in fakedict['eriver']

def test_river_by_station_number():
    a = 1
    fakestats = geo.rivers_by_station_number(Fake_stations_for_testing, a)
    assert fakestats == [('ariver', 2)]

    a += 1
    fakestats = geo.rivers_by_station_number(Fake_stations_for_testing, a)
    assert fakestats.index(('ariver', 2)) < fakestats.index(('criver', 1))
    assert fakestats.index(('ariver', 2)) < fakestats.index(('driver', 1))
    assert fakestats.index(('ariver', 2)) < fakestats.index(('eriver', 1))
    
    assert len(fakestats) == 6
    assert ("friver", 1) in fakestats
    assert ('briver', 1) in fakestats
    assert ('driver', 1) in fakestats