# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""Unit test for the station module"""

from floodsystem.station import MonitoringStation, inconsistent_typical_range_stations

def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town


def test_typical_range_consistent():

    #create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    river = "River X"
    town = "My Town"

    trange = (-2.3, 3.4445)
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    assert s.typical_range_consistent() == True

    trange = (3.4445, -2.3)
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    assert s.typical_range_consistent() == False

    trange = None
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    assert s.typical_range_consistent() == False

def test_inconsistent_typical_range_stations():
    s_id = "test-s-id"
    m_id = "test-m-id"
    coord = (-2.0, 4.0)
    river = "River X"
    town = "My Town"

    stations = [
        MonitoringStation(s_id, m_id, "Station1", coord, (1, 3), river, town),
        MonitoringStation(s_id, m_id, "Station2", coord, (1.00534, 3.5673), river, town),
        MonitoringStation(s_id, m_id, "Station3", coord, (3, 1), river, town),
        MonitoringStation(s_id, m_id, "Station4", coord, None, river, town),
        MonitoringStation(s_id, m_id, "Station5", coord, "not a container", river, town),
    ]

    inconsistents = inconsistent_typical_range_stations(stations)

    assert len(inconsistents) == 3
    assert stations[0] not in inconsistents
    assert stations[1] not in inconsistents
    assert stations[2] in inconsistents
    assert stations[3] in inconsistents
    assert stations[4] in inconsistents

def test_relative_water_level():
    s_id = "test-s-id"
    m_id = "test-m-id"
    coord = (-2.0, 4.0)
    river = "River X"
    town = "My Town"
    stations = [
        MonitoringStation(s_id, m_id, "Station1", coord, (1, 3), river, town),
        MonitoringStation(s_id, m_id, "Station2", coord, (0, 1), river, town),
        MonitoringStation(s_id, m_id, "Station3", coord, (0, 1), river, town),
        MonitoringStation(s_id, m_id, "Station4", coord, (3, 1), river, town),
        MonitoringStation(s_id, m_id, "Station5", coord, "not a container", river, town),
    ]

    stations[0].latest_level = 2
    stations[1].latest_level = 1
    stations[2].latest_level = 34
    stations[3].latest_level = 23
    stations[4].latest_level = 235

    assert stations[0].relative_water_level() == 0.5
    assert stations[1].relative_water_level() == 1
    assert stations[2].relative_water_level() == 34
    assert stations[3].relative_water_level() == None
    assert stations[4].relative_water_level() == None