import datetime

from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.plot import plot_water_level
from floodsystem.flood import stations_highest_rel_level

def run():

    # Build list of stations
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)

    # Fetch data over past 10 days
    for station in stations_highest_rel_level(stations, 5):
        dt = 10
        dates, levels = fetch_measure_levels(
            station.measure_id, dt=datetime.timedelta(days=dt))

        plot_water_level(station, dates, levels)

if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")
    run()
     