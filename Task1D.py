from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station, stations_by_river

def run():
    """Requirements for Task 1D"""

    # Build list of stations
    stations = build_station_list()

    # Print number of stations
    print("Number of stations: {}".format(len(stations)))
    rivers = rivers_with_station(stations)
    print(len(rivers))
    print(sorted(rivers)[:10])
    
    by_river = stations_by_river(stations)
    print(sorted([station.name for station in by_river["River Aire"]]))

    print(sorted([station.name for station in by_river["River Cam"]]))

    print(sorted([station.name for station in by_river["River Thames"]]))


if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    run()