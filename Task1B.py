from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance
def run():
    """Requirements for Task 1B"""

    # Build list of stations
    stations = build_station_list()

    # Print number of stations
    print("Number of stations: {}".format(len(stations)))

    distance_sorted_stations = stations_by_distance(stations, (52.2053, 0.1218))

    first_ten = [(station.name, station.town, p) for station, p in distance_sorted_stations[:10]]
    last_ten = [(station.name, station.town, p) for station, p in distance_sorted_stations[-10:]]
    
    print("Closest 10 stations:", first_ten)
    print("Furthest 10 stations:", last_ten)


if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    run()