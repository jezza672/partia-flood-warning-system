import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from datetime import datetime
from floodsystem.analysis import polyfit

def test_polyfit():
    x = [datetime(2020, 2, 20, 0, 0, 1),
        datetime(2020, 2, 20, 0, 0, 2),
        datetime(2020, 2, 20, 0, 0, 3),
        datetime(2020, 2, 20, 0, 0, 4),
        datetime(2020, 2, 20, 0, 0, 5), 
        datetime(2020, 2, 20, 0, 0, 6), 
        datetime(2020, 2, 20, 0, 0, 7),
        datetime(2020, 2, 20, 0, 0, 8),
        datetime(2020, 2, 20, 0, 0, 9), 
        datetime(2020, 2, 20, 0, 0, 10)]
    y = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]
    poly, delta = polyfit(x, y, 2)
    assert round(poly(matplotlib.dates.date2num(x[1]) - delta), 4) == y[1]
