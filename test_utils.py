"""Unit test for the utils module"""

import floodsystem.utils


def test_sort():
    """Test sort container by specific index"""

    a = (10, 3, 3)
    b = (5, 1, -1)
    c = (1, -3, 4)
    list0 = (a, b, c)

    # Test sort on 1st entry
    list1 = floodsystem.utils.sorted_by_key(list0, 0)
    assert list1[0] == c
    assert list1[1] == b
    assert list1[2] == a

    # Test sort on 2nd entry
    list1 = floodsystem.utils.sorted_by_key(list0, 1)
    assert list1[0] == c
    assert list1[1] == b
    assert list1[2] == a

    # Test sort on 3rd entry
    list1 = floodsystem.utils.sorted_by_key(list0, 2)
    assert list1[0] == b
    assert list1[1] == a
    assert list1[2] == c


def test_reverse_sort():
    """Test sort container by specific index (reverse)"""

    a = (10, 3, 3)
    b = (5, 1, -1)
    c = (1, -3, 4)
    list0 = (a, b, c)

    # Test sort on 1st entry
    list1 = floodsystem.utils.sorted_by_key(list0, 0, reverse=True)
    assert list1[0] == a
    assert list1[1] == b
    assert list1[2] == c

    # Test sort on 2nd entry
    list1 = floodsystem.utils.sorted_by_key(list0, 1, reverse=True)
    assert list1[0] == a
    assert list1[1] == b
    assert list1[2] == c

    # Test sort on 3rd entry
    list1 = floodsystem.utils.sorted_by_key(list0, 2, reverse=True)
    assert list1[0] == c
    assert list1[1] == a
    assert list1[2] == b


def test_distance():
    """Test the distance function"""
    p1 = (10, 10)
    p2 = (5, 5)
    assert abs(round(floodsystem.utils.calculate_distance(p1, p2), 0)) == 783
    '''Distance between memorial court, clare college and the cambridge engineering dept :) '''
    p1 = (52.21, 0.11)
    p2 = (52.20, 0.12)
    assert round(floodsystem.utils.calculate_distance(p1, p2), 0) == round(1.3, 0)
    '''Distance between my house and Jeremy's house (we're pretty close)'''
    p1 = (51.61, -0.26)
    p2 = (51.59, -0.38)
    assert round(floodsystem.utils.calculate_distance(p1, p2), 0) == round(8.9, 0)
