# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""
from typing import Tuple, List, Set, Dict

from .utils import calculate_distance, sorted_by_key
from .station import MonitoringStation


def stations_by_distance(stations : List[MonitoringStation], p : Tuple[float, float]) -> List[Tuple[MonitoringStation, float]]:
    """Return a list of tuples of the form (station, distance), sorted by distance from the given point."""
    try:
        if len(p) < 2:
            raise TypeError("p not long enough")
    except TypeError:
        raise TypeError("p not long enough")
    
    tuple_list = [(station, (calculate_distance(station.coord, p))) for station in stations]
    return sorted_by_key(tuple_list, 1)

def stations_within_radius(stations : List[MonitoringStation], centre : Tuple[float, float], r: float) -> List[MonitoringStation]:
    """Return a list of stations within a given radius of a given point."""
    try:
        if len(centre) < 2:
            raise TypeError("centre not long enough")
    except TypeError:
        raise TypeError("centre not long enough")  
    return [station for station in stations if calculate_distance(station.coord, centre) < r]

def rivers_with_station(stations : List[MonitoringStation]) -> Set[str]:
    """Return a set of rivers that are assosiated with the given stations."""
    s = set() 
    for station in stations:
        if isinstance(station.river, str):
            s.add(station.river)
        else:
            [s.add(x) for x in station.river]
    return s

def stations_by_river(stations : MonitoringStation) -> Dict[str, MonitoringStation]:
    """Return a dictionary of stations (value) by river (key)"""
    d = {}
    for station in stations:
        if isinstance(station.river, str):
            try:
                d[station.river].append(station)
            except:
                d[station.river] = [station]
        else:
            for river in station.river:
                try:
                    d[river].append(station)
                except:
                    d[river] = [station]
    return d

def rivers_by_station_number(stations : MonitoringStation, N : int) -> List[Tuple[str, int]]:
    """Return a list of the N rivers with the most stations, in tuples of the form (river, number of stations)"""
    l = sorted_by_key([(river, len(stats)) for river, stats in stations_by_river(stations).items()], 1, True)
    i = N-1
    while l[i][1] == l[i + 1][1]:
        i += 1 
        if i+1 >= len(l):
            return l
    return l[:i + 1]

def stations_by_town(stations : MonitoringStation) -> Dict[str, MonitoringStation]:
    """Return a dictionary of stations (value) by town (key)"""
    d = {}
    for station in stations:
        if isinstance(station.town, str):
            try:
                d[station.town].append(station)
            except:
                d[station.town] = [station]
    return d