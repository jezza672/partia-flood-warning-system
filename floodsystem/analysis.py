"""This module provides functions to perform analysis on monitoring station data"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.dates import date2num
from .datafetcher import fetch_measure_levels
import datetime

def polyfit(dates, levels, p):
    """Fits a polynomial of order p to the data provided."""

    x = np.array([date2num(x) for x in dates])
    y = levels

    # Using shifted x values, find coefficient of best-fit
    # polynomial f(x) of degree 4
    p_coeff = np.polyfit(x - x[0], y, p)

    # Convert coefficient into a polynomial that can be evaluated
    # e.g. poly(0.3)
    poly = np.poly1d(p_coeff)

    return poly, x[0]


def rising(station):
    dt = 2
    try:
        dates, levels = fetch_measure_levels(
            station.measure_id, dt=datetime.timedelta(days=dt))
        if len(dates) <= 0 or len(levels) <= 0:
            return False
        poly, delta = polyfit(dates, levels, 3)

        if poly(date2num(dates[-1]) - delta) > poly(date2num(dates[-2]) - delta):
            return True
        return False
    except (KeyError, TypeError):
        return False

