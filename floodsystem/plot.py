import matplotlib.pyplot as plt
from matplotlib.dates import date2num, num2date
import numpy as np
from .analysis import polyfit
from .station import MonitoringStation

from datetime import datetime, timedelta
from typing import List

def plot_water_level(station : MonitoringStation, dates : List[datetime], levels : List[float]) -> None:
    plt.plot(dates, levels, label = 'Water Level')
    
    if station.typical_range_consistent() == True:
         plt.axhline(station.typical_range[0], color='green', label="Typical Low")
         plt.axhline(station.typical_range[1], color='red', label= 'Typical High')
    else:
        pass

    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title(station.name)
    plt.legend()
    plt.tight_layout()
    plt.show()

def plot_water_level_with_fit(station : MonitoringStation, dates : List[datetime], levels : List[float], p : int) -> None:
    # Plot original data points
    plt.plot(dates, levels, '.', label="Level Data")

    poly, delta = polyfit(dates, levels, p)

    # Plot polynomial fit at 30 points along interval (note that polynomial
    # is evaluated using the shift x)
    x1 = np.linspace(date2num(dates[0]), date2num(dates[-1]), 30)
    plt.plot(num2date(x1), poly(x1 - delta), label="Polynomial Fit")

    plt.axhline(station.typical_range[0], label="Typical Low", color="green")
    plt.axhline(station.typical_range[1], label="Typical High", color="red")

    plt.xlabel('Date')
    plt.ylabel('Water level (m)')
    
    plt.xticks(rotation=45)
    plt.title(station.name)

    plt.legend()
    plt.tight_layout()

    # Display plot
    plt.show()
