"""This module provides functions for checking for floods"""

from .station import MonitoringStation
from .utils import sorted_by_key
from .geo import stations_by_town
from .analysis import rising

from typing import List, Tuple

def stations_level_over_threshold(stations : MonitoringStation, tol: float) -> List[Tuple[MonitoringStation, float]]:
    """Returns a list of tuples of stations and their relative water levels for stations over the threshold, in descending order of relative water level."""
    return sorted_by_key([(station, station.relative_water_level()) for station in stations if station.relative_water_level() != None and station.relative_water_level() > tol], 1, True)


def stations_highest_rel_level(stations : MonitoringStation, N : int):
    """Returns the N most at risk stations of flooding"""
    return sorted([station for station in stations if station.relative_water_level() != None], key = lambda x: x.relative_water_level(), reverse = True)[:N]

def towns_most_at_risk(stations : MonitoringStation, tols: List[float]) -> List[Tuple[str, str]]:
    at_risk = []
    for town, town_stations in stations_by_town(stations).items():
        risk = max([get_risk(station, 1.0, 1.5) for station in town_stations])
        if risk > 0:
            at_risk.append((town, ["", "Low", "Medium", "High", "Severe"][risk]))
    return at_risk
        

def get_risk(station, lower, upper):
    risk = 0
    if station.relative_water_level() is None:
        return risk
    if station.relative_water_level() > lower:
        risk = 1 if risk < 1 else risk
    if station.relative_water_level() > lower and rising(station):
        risk = 2 if risk < 2 else risk
    if station.relative_water_level() > upper:
        risk = 3 if risk < 3 else risk
    if station.relative_water_level() > upper and rising(station):
        risk = 4 if risk < 4 else risk
    return risk